import filecmp
import hashlib
import json
import os
import shutil
import StringIO
import tempfile

import keyczar
import keyczar.keyczar
import keyczar.keyczart
import keyczar.readers
import keyczar.writers
import keyczar.util


class Cryptic(object):
    """
    TODO:
    this class currently assumes that 
    the backend store is somewhere on a locally mounted file system
    we need to generalize that, e.g.
    if the files are to be stored on Amazon S3
    """

    def __init__(self):
        pass


    def name(self, value=None):
        if value is not None:
            self._name = value
        if not hasattr(self, '_name'):
            self._name = None
        return self._name


    def location(self, value=None):
        if value is not None:
            self._location = value
        if not hasattr(self, '_location'):
            self._location = None
        return self._location


    def crypter(self, value=None):
        if value is not None:
            self._crypter = value
        if not hasattr(self, '_crypter'):
            self._crypter = None
        return self._crypter


    def keyDir(self):
        return os.path.join(self.location(), 'keyStore')

    def fileDir(self):
        return os.path.join(self.location(), 'fileStore')


    def initialize(self):
        """
        initialize using existing storage backend
        """
        crypter = keyczar.keyczar.Crypter.Read(self.keyDir())
        self.crypter(crypter)
        return


    def initializeStorageBackend(self):
        """
        initialize the storage backend
        should only be called once, ever
        """
        self.initializeKeyStore()
        self.initializeFileStore()
        return

    def hasInitializedKeyStore(self):
        return os.path.exists(self.keyDir())

    def initializeKeyStore(self):
        self.createKeyStore()
        self.addKey()
        return

    def hasInitializedFileStore(self):
        return os.path.exists(self.fileDir())

    def initializeFileStore(self):
        self.createFileStore()
        return

    def createFileStore(self):
        os.makedirs(self.fileDir())

    def createKeyStore(self):
        os.makedirs(self.keyDir())
        keyczar.keyczart.main(['create','--location=%s' % self.keyDir(),
                               '--purpose=crypt','--name=%s' % self.name()])
        return


    def addKey(self):
        keyczar.keyczart.main(['addkey', '--location=%s' % self.keyDir(),
                               '--status=primary',
                               '--size=256'])
        return


    def hasData(self, context):
        dataFilePath = self.constructEncryptedPath(context)
        return os.path.exists(dataFilePath)


    def removeData(self, context):
        if not self.hasData(context):
            raise IOError('data does not exist')
        dataFilePath = self.constructEncryptedPath(context)
        os.unlink(dataFilePath)
        return


    def storeFile(self, filePath, context=None):

        with open(filePath, 'r') as fileData:
            self.storeDataStream(fileData, context=context)
            pass

        return


    def storeDataStream(self, dataStream, context=None):

        data = dataStream.read()
        self.storeData(data, context=context)
        return


    def storeData(self, data, context=None):

        encryptedFilePath = self.constructEncryptedPath(context)
        with open(encryptedFilePath, 'wb') as encryptedOutput:
            crypter = self.crypter()
            writer = crypter.CreateEncryptingStreamWriter(encryptedOutput, encoder=None)
            writer.write(data)
            writer.flush()
            writer.close()
            pass

        return


    def retrieveFile(self, targetFilePath, context=None):
        with open(targetFilePath, 'wb') as f:
            self.retrieveData(f, context=context)
            pass
        return


    def retrieveData(self, outputStream, context=None):
        encryptedFilePath = self.constructEncryptedPath(context)
        crypter = self.crypter()

        active_ciphertext = keyczar.util.ReadFile(os.path.join(encryptedFilePath))
        decryption_stream = crypter.CreateDecryptingStreamReader(
            StringIO.StringIO(active_ciphertext),
            decoder=None,
            buffer_size=8192)

        # this is a crappy inteface from keyczar
        buffer = True
        while buffer or buffer is None:
            buffer = decryption_stream.read(8192)
            if buffer:
                outputStream.write(buffer)
            pass
        decryption_stream.close()
        return


    def constructEncryptedPath(self, context):

        if context is None:
            context = {}
        contextHash = self.hashData(json.dumps(context))
            
        return os.path.join(self.fileDir(), contextHash)


    def hashDataStream(self, dataStream, hashFunction=None):

        data = dataStream.read()
        self.hashData(data, hashFunction = hashFunction)

        return


    def hashData(self, data, hashFunction=None):

        if hashFunction is None:
            hashFunction = hashlib.sha512

        return hashFunction(data).hexdigest()


    def encryptData(self, data):
        crypter = self.crypter()
        return crypter.Encrypt(data)


    def decryptData(self, data):
        crypter = self.crypter()
        return crypter.Decrypt(data)


    # END class Cryptic
    pass
