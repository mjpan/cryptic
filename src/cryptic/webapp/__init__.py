import tornado
import tornado.ioloop
import tornado.web

import json
import logging
import os
import StringIO
import traceback


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")


class BaseHandler(tornado.web.RequestHandler):

    def initialize(self, cryptic=None):
        self.cryptic = cryptic
        return


    def onValidRequestType(self):
        response = {}

        try:
            self.validateRequest(response)
            self.handleRequest(response)
        except Exception, e:
            logging.warn(traceback.format_exc())
            response['status'] = 'error'
            response['reason'] = str(e)
            pass
        self.sendResponse(response)
        self.finish()
        return


    def sendResponse(self, response):
        if response['status'] == 'success':
            self.sendSuccessResponse(response)
            pass
        else:
            self.sendErrorResponse(response)
            pass
        return


    def sendSuccessResponse(self, response):
        self.write(json.dumps(response))
        return


    def sendErrorResponse(self, response):
        self.write(json.dumps(response))
        return


    # END class BaseHandler
    pass


class FileStoreHandler(BaseHandler):

    def get(self):
        self.send_error(403)
        return

    def post(self):
        self.onValidRequestType()
        return

    def extractFileContext(self):
        context = json.loads(self.get_argument('context'))
        return context


    # END class FileStoreHandler
    pass


class UploadHandler(FileStoreHandler):


    def validateRequest(self, response):

        # TODO:
        # ensure that there's a file
        # ensure that there's a context

        return


    def handleRequest(self, response):

        context = self.extractFileContext()

        if self.cryptic.hasData(context):
            raise Exception('data already exists for at context')

        fileInfo = self.request.files['files'][0]
        fileData = fileInfo['body']
        fileDataStream = StringIO.StringIO(fileData)
        self.cryptic.storeData(fileDataStream, context=context)

        response['status'] = 'success'
        response['context'] = json.dumps(context)
        
        return


    # END class UploadHandler
    pass


class DownloadHandler(FileStoreHandler):

    def validateRequest(self, response):

        # TODO:
        # ensure there's a context

        return


    def handleRequest(self, response):

        context = self.extractFileContext()

        dataStream = StringIO.StringIO()
        self.cryptic.retrieveData(dataStream, context=context)

        self.dataToSend = dataStream
        response['status'] = 'success'

        return

    def sendSuccessResponse(self, response):
        dataToSend = self.dataToSend.getvalue()
        self.write(dataToSend)
        return

    def sendErrorResponse(self, response):
        self.set_status(404)
        self.write(json.dumps(response))
        return


    # END class DownloadHandler
    pass



class CryptHandler(BaseHandler):

    def get(self):
        self.onValidRequestType()
        return


    def validateRequest(self, response):
        return


    # END class CryptHandler
    pass


class HashDataHandler(CryptHandler):

    def handleRequest(self, response):
        data = self.get_argument('data')
        result = self.cryptic.hashData(data)
        response['status'] = 'success'
        response['result'] = result
        return


    # END class CryptHandler
    pass


class VerifyHashHandler(CryptHandler):


    def handleRequest(self, response):

        data = self.get_argument('data')
        hashedData = self.cryptic.hashData(data)

        expectedHash = self.get_argument('hash')
        response['status'] = 'success'
        response['result'] = expectedHash == hashedData

        return


    # END class VerifyHashHandler
    pass


class EncryptHandler(CryptHandler):

    def handleRequest(self, response):

        data = self.get_argument('data')
        result = self.cryptic.encryptData(data)

        response['status'] = 'success'
        response['result'] = result

        return

    # END class EncryptHandler
    pass


class DecryptHandler(CryptHandler):

    def handleRequest(self, response):

        data = self.get_argument('data')
        result = self.cryptic.decryptData(data)

        response['status'] = 'success'
        response['result'] = result

        return

    # END class DecryptHandler
    pass
