#!/bin/python

import json
import shutil
import sys

import requests

import argparse


parser = argparse.ArgumentParser(description='sample webapp client')
parser.add_argument('--port', dest='port', type=int, default=8888, help='the port that the server runs')
parser.add_argument('--host', dest='host', default='localhost', help='the host that the server runs')
parser.add_argument('--output', dest='output', default='foo.pdf', help='location to save the downloaded file')

args = parser.parse_args()


def main():
    url = 'http://%s:%s/download' % (args.host, args.port)

    context = {'foo':'bars'}
    response = requests.post(url, data={'context':json.dumps(context)}, stream=True)

    if response.status_code == 200:
        # save to disk
        with open(args.output, 'wb') as out_file:
            response.raw.decode_content = True
            shutil.copyfileobj(response.raw, out_file)
        pass
    elif response.status_code == 404:
        print "server responded with file not found"
    return

if __name__ == '__main__':
    main()
    pass
