import os
import sys

import tornado.ioloop
import tornado.web

sys.path.insert(0, 'src')
import cryptic
import cryptic.webapp


import argparse


parser = argparse.ArgumentParser(description='sample webapp')
parser.add_argument('--port', dest='port', type=int, default=8888, help='the port that the server runs')
parser.add_argument('--name', dest='keyczarName', default='demo', help='name to provide to keyczar')
parser.add_argument('--location', dest='keyczarLocation', default='webserver', help='location to provide to keyczar')

args = parser.parse_args()


def make_app():

    # TODO:
    # these values should come from config file
    theCryptic = cryptic.Cryptic()
    theCryptic.name(args.keyczarName)
    theCryptic.location(args.keyczarLocation)

    if not theCryptic.hasInitializedKeyStore():
        theCryptic.initializeKeyStore()
    if not theCryptic.hasInitializedFileStore():
        theCryptic.initializeFileStore()

    theCryptic.initialize()

    return tornado.web.Application([
        (r"/", cryptic.webapp.MainHandler),
        (r"/upload", cryptic.webapp.UploadHandler, dict(cryptic=theCryptic)),
        (r"/download", cryptic.webapp.DownloadHandler, dict(cryptic=theCryptic)),
        (r"/hash", cryptic.webapp.HashDataHandler, dict(cryptic=theCryptic)),
        (r"/verifyHash", cryptic.webapp.VerifyHashHandler, dict(cryptic=theCryptic)),
        (r"/encrypt", cryptic.webapp.EncryptHandler, dict(cryptic=theCryptic)),
        (r"/decrypt", cryptic.webapp.DecryptHandler, dict(cryptic=theCryptic)),
    ])

def main():
    app = make_app()
    app.listen(args.port)
    tornado.ioloop.IOLoop.current().start()
    return

if __name__ == "__main__":
    main()

