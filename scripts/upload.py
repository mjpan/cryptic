#!/bin/python

import json
import sys


import requests


import argparse


parser = argparse.ArgumentParser(description='sample webapp client')
parser.add_argument('--port', dest='port', type=int, default=8888, help='the port that the server runs')
parser.add_argument('--host', dest='host', default='localhost', help='the host that the server runs')
args = parser.parse_args()


def main():
    url = 'http://%s:%s/upload' % (args.host, args.port)

    context = {'foo':'bar'}

    fileName = 'keyczar05b.pdf'
    filePath = 'docs/%s' % fileName

    with open(filePath, 'rb') as f:
        files = [
            ('files', (fileName, f))
            ]
        response = requests.post(url, files=files, data={'context':json.dumps(context)})
        print response.json()
    return

if __name__ == '__main__':
    main()
    pass
