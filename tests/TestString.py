import unittest

import os
import shutil
import tempfile

import keyczar
import keyczar.keyczar
import keyczar.keyczart

class TestString(unittest.TestCase):


    def setUp(self):

        # setup the temp dir
        self.dir = tempfile.mkdtemp()

        keyczar.keyczart.main(['create','--location=%s' % self.dir,
                               '--purpose=crypt','--name=unittest'])
        keyczar.keyczart.main(['addkey', '--location=%s' % self.dir,
                               '--status=primary'])
        return

    def tearDown(self):
        # teardown the temp dir
        if hasattr(self, 'dir') and self.dir is not None and os.path.exists(self.dir):
            shutil.rmtree(self.dir)
        return


    def verifyRoundtripEncryption(self, s):
        crypter = keyczar.keyczar.Crypter.Read(self.dir)
        s_encrypted = crypter.Encrypt(s, encoder=None)
        s_decrypted = crypter.Decrypt(s_encrypted, decoder=None)
        self.assertEquals(s, s_decrypted)
        self.assertNotEquals(s, s_encrypted)
        return

    def testEncryptString(self):
        self.verifyRoundtripEncryption('some random string')

        return



    # END class TestString
    pass
