import unittest

import filecmp
import hashlib
import json
import os
import shutil
import tempfile

import StringIO

import cryptic

import subprocess
import requests
import time

class TestWebappClient(unittest.TestCase):

    def setUp(self):

        self.dir = tempfile.mkdtemp()

        self.crypticName = 'unittest'
        self.crypticLocation = self.dir
        self.setupCryptic()

        self.host = 'localhost'
        self.port = '8888'

        # start the webapp
        args = ['python', 'scripts/webapp.py', '--name', self.crypticName, '--location', self.crypticLocation, '--port', self.port]
        self.webappProcess = subprocess.Popen(args)
        time.sleep(1)


        return


    def tearDown(self):

        self.webappProcess.terminate()
        self.webappProcess.wait()

        # teardown the temp dir
        if hasattr(self, 'dir') and self.dir is not None and os.path.exists(self.dir):
            shutil.rmtree(self.dir)

        return


    def setupCryptic(self):
        theCryptic = cryptic.Cryptic()
        theCryptic.name(self.crypticName)
        theCryptic.location(self.crypticLocation)
        theCryptic.initializeStorageBackend()
        theCryptic.initialize()
        self.cryptic = theCryptic
        return


    def testUpload_newData(self):
        raise NotImplementedError


    def testUpload_existingData(self):
        raise NotImplementedError


    def testDownload_existingData(self):
        raise NotImplementedError



    def testDownload_noData(self):
        url = 'http://%s:%s/download' % (self.host, self.port)
        context = {'context':'does not exist'}
        response = requests.post(url, data={'context':json.dumps(context)}, stream=True)
        self.assertEquals(response.status_code, 404)
        return


    def testHashData_post(self):
        url = 'http://%s:%s/hash' % (self.host, self.port)
        dataToHash = 'data to hash'
        response = requests.post(url, data={'data':dataToHash}, stream=True)
        self.assertEquals(response.status_code, 405)
        return


    def testHashData(self):
        url = 'http://%s:%s/hash' % (self.host, self.port)
        dataToHash = 'data to hash'
        response = requests.get(url, data={'data':dataToHash}, stream=True)

        expected = {
            'status':'success',
            'result':self.cryptic.hashData(dataToHash)
            }

        self.assertEquals(response.json(), expected)
        return



    def testVerifyHash_post(self):
        url = 'http://%s:%s/verifyHash' % (self.host, self.port)
        dataToHash = 'data to hash'
        hashedData = self.cryptic.hashData(dataToHash)
        response = requests.post(url, data={'data':dataToHash, 'hash':hashedData}, stream=True)
        self.assertEquals(response.status_code, 405)
        return


    def testVerifyHash(self):
        url = 'http://%s:%s/verifyHash' % (self.host, self.port)

        dataToHash = 'data to hash'
        hashedData = self.cryptic.hashData(dataToHash)

        response = requests.get(url, data={'data':dataToHash, 'hash':hashedData}, stream=True)
        
        expected = {
            'status':'success',
            'result':True
            }
        self.assertEquals(response.json(), expected)


        response = requests.get(url, data={'data':dataToHash, 'hash':'foo'}, stream=True)
        
        expected = {
            'status':'success',
            'result':False
            }
        self.assertEquals(response.json(), expected)

        return


    def testEncryptData_post(self):
        url = 'http://%s:%s/encrypt' % (self.host, self.port)
        dataToEncrypt = 'data to encrypt'
        encryptedData = self.cryptic.encryptData(dataToEncrypt)
        response = requests.post(url, data={'data':dataToEncrypt}, stream=True)
        self.assertEquals(response.status_code, 405)
        return


    def testDecryptData_post(self):
        url = 'http://%s:%s/decrypt' % (self.host, self.port)
        dataToDecrypt = 'data to decrypt'
        response = requests.post(url, data={'data':dataToDecrypt}, stream=True)
        self.assertEquals(response.status_code, 405)
        return


    def testRoundtrypEncryptData(self):
        url = 'http://%s:%s/encrypt' % (self.host, self.port)
        dataToEncrypt = 'data to encrypt'

        response = requests.get(url, data={'data':dataToEncrypt}, stream=True)
        responseJson = response.json()

        self.assertEquals(responseJson['status'], 'success')

        encryptedData = responseJson['result']

        url = 'http://%s:%s/decrypt' % (self.host, self.port)

        response = requests.get(url, data={'data':encryptedData}, stream=True)
        responseJson = response.json()

        self.assertEquals(responseJson['status'], 'success')
        decryptedData = responseJson['result']

        self.assertEquals(dataToEncrypt, decryptedData)
        self.assertFalse(encryptedData == decryptedData)

        return



    # END class TestWebappClient
    pass


