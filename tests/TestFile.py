import unittest

import filecmp
import os
import shutil
import tempfile

import StringIO

import keyczar
import keyczar.keyczar
import keyczar.keyczart
import keyczar.readers
import keyczar.writers
import keyczar.util

class TestFile(unittest.TestCase):


    def setUp(self):

        # setup the temp dir
        self.dir = tempfile.mkdtemp()

        os.makedirs(self.keyDir())

        keyczar.keyczart.main(['create','--location=%s' % self.keyDir(),
                               '--purpose=crypt','--name=unittest'])
        keyczar.keyczart.main(['addkey', '--location=%s' % self.keyDir(),
                               '--status=primary'])
        return


    def tearDown(self):
        # teardown the temp dir
        if hasattr(self, 'dir') and self.dir is not None and os.path.exists(self.dir):
            shutil.rmtree(self.dir)
        return

    def keyDir(self):
        return os.path.join(self.dir, 'keyczar')

    def verifyRoundtripEncryption(self, filepath):

        # fileReader = keyczar.readers.CreateReader(filepath)
        

        crypter = keyczar.keyczar.Crypter.Read(self.keyDir())

        encryptedFh, encryptedFilePath = tempfile.mkstemp(dir=self.dir)
        with open(filepath, 'r') as fileData:
            with open(encryptedFilePath, 'wb') as encryptedOutput:
                writer = crypter.CreateEncryptingStreamWriter(encryptedOutput, encoder=None)
                writer.write(fileData.read())
                writer.flush()
                writer.close()
            pass


        active_ciphertext = keyczar.util.ReadFile(os.path.join(encryptedFilePath))
        decryption_stream = crypter.CreateDecryptingStreamReader(
            StringIO.StringIO(active_ciphertext),
            decoder=None,
            buffer_size=8192)
        decryptedFh, decryptedFilePath = tempfile.mkstemp(dir=self.dir)
        with open(decryptedFilePath, 'wb') as decryptedOutput:
            # this is a crappy inteface from keyczar
            buffer = True
            while buffer or buffer is None:
                buffer = decryption_stream.read(8192)
                if buffer:
                    decryptedOutput.write(buffer)
            pass
        decryption_stream.close()

        self.assertTrue(filecmp.cmp(filepath, decryptedFilePath))
        self.assertFalse(filecmp.cmp(filepath, encryptedFilePath))

        return

    def testEncryptFile(self):
        filepath = os.path.join('docs', 'keyczar05b.pdf')
        self.verifyRoundtripEncryption(filepath)
        return



    # END class TestFile
    pass
