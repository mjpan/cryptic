import unittest

import filecmp
import hashlib
import json
import os
import shutil
import tempfile

import StringIO

import cryptic
import cryptic.webapp
import mock


class TestBaseHandler(unittest.TestCase):

    def setUp(self):
        self.dir = tempfile.mkdtemp()
        self.initializeObjectToTest()
        return

    def tearDown(self):
        # teardown the temp dir
        if hasattr(self, 'dir') and self.dir is not None and os.path.exists(self.dir):
            shutil.rmtree(self.dir)
        return

    # END class TestBaseHandler
    pass


class TestFileStoreHandler(TestBaseHandler):

    def initializeObjectToTest(self):
        mock.setupMockFunctionsOnClass(cryptic.webapp.FileStoreHandler, ['__init__'], {})
        self.objectToTest = cryptic.webapp.FileStoreHandler()
        return

    # get
    def testHttpGet(self):

        mock.setupMockFunctionsOnObject(self.objectToTest, ['send_error'], {})
        self.objectToTest.get()
        expected = {
            'send_error':[((403,),{})]
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)
        return

    # post
    def testHttpPost(self):

        mock.setupMockFunctionsOnObject(self.objectToTest, ['validateRequest', 'handleRequest', 'sendResponse', 'finish'], {})
        self.objectToTest.post()
        expected = {
            'validateRequest':[(({},),{})],
            'handleRequest':[(({},),{})],
            'sendResponse':[(({},),{})],
            'finish':[((),{})]
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)


        return


    def testSendResponse(self):

        mock.setupMockFunctionsOnObject(self.objectToTest, ['sendSuccessResponse', 'sendErrorResponse'], {})
        response = {'status':'success'}
        self.objectToTest.sendResponse(response)
        expected = {
            'sendSuccessResponse':[((response,),{})],
            'sendErrorResponse':[]
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)
        return

    def testSendSuccessResponse(self):

        mock.setupMockFunctionsOnObject(self.objectToTest, ['write'], {})
        response = {'status':'success'}
        self.objectToTest.sendSuccessResponse(response)
        expected = {
            'write':[((json.dumps(response),),{})],
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)
        return


    def sendErrorResponse(self):
        mock.setupMockFunctionsOnObject(self.objectToTest, ['write'], {})
        response = {'status':'error'}
        self.objectToTest.sendSuccessResponse(response)
        expected = {
            'write':[((json.dumps(response),),{})],
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)
        return


    def testExtractFileContext(self):
        context = {'foo':'bar'}
        self.objectToTest._finished = False
        mock.setupMockFunctionsOnObject(self.objectToTest, ['get_argument'], {'get_argument':json.dumps(context)})
        response = {'status':'error'}
        self.objectToTest.extractFileContext()
        expected = {
            'get_argument':[(('context',),{})],
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)

        return

    # END TestFileStoreHandler
    pass


class TestDownloadHandler(TestFileStoreHandler):

    def setUp(self):
        TestFileStoreHandler.setUp(self)
        return

    def tearDown(self):
        TestFileStoreHandler.tearDown(self)
        return

    def initializeObjectToTest(self):
        mock.setupMockFunctionsOnClass(cryptic.webapp.DownloadHandler, ['__init__'], {})
        self.objectToTest = cryptic.webapp.DownloadHandler()
        return

    def testValidateRequest(self):
        raise NotImplementedError


    def testHandleRequest(self):

        context = {'foo':'bar'}
        mock.setupMockFunctionsOnObject(self.objectToTest, ['extractFileContext'], {'extractFileContext':context})

        mockCryptic = mock.Mock()
        mock.setupMockFunctionsOnObject(mockCryptic, ['retrieveData'], {})
        self.objectToTest.cryptic = mockCryptic

        response = {}

        self.objectToTest.handleRequest(response)

        expectedResponse = {'status':'success'}
        self.assertEquals(expectedResponse, response)

        expected = {
            'extractFileContext':[((),{})]
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)


        self.assertTrue(isinstance(self.objectToTest.dataToSend, StringIO.StringIO))

        # assert that it calls extractFileContext, which returns context
        # assert that obj.cryptic calls retrieveData, and passes StringIO instance and context
        # assert that StringIO instance is set as dataStream
        # assert that response obj has status = success
        return

    def testSendSuccessResponse(self):
        # assert that dataToSend.getvalue is called
        # assert that write is called
        mockDataValue = mock.Mock()
        mockDataToSend = mock.Mock()
        mock.setupMockFunctionsOnObject(mockDataToSend, ['getvalue'], {'getvalue':mockDataValue})
        self.objectToTest.dataToSend = mockDataToSend

        mock.setupMockFunctionsOnObject(self.objectToTest, ['write'], {})

        response = {'status':'success'}
        self.objectToTest.sendSuccessResponse(response)

        expected = {
            'getvalue':[((),{})]
            }
        mock.verifyExpectedCalls(self, mockDataToSend, expected)

        expected = {
            'write':[((mockDataValue,),{})]
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)
        return


    def testSendErrorResponse(self):
        # assert that set_status is called
        # assert that write is called

        mock.setupMockFunctionsOnObject(self.objectToTest, ['set_status','write'], {})
        response = {'status':'error'}
        self.objectToTest.sendErrorResponse(response)
        expected = {
            'set_status':[((404,),{})],
            'write':[((json.dumps(response),),{})],
            }
        mock.verifyExpectedCalls(self, self.objectToTest, expected)

        return



    # END class TestDownloadHandler
    pass


class TestUploadHandler(unittest.TestCase):

    def setUp(self):
        self.dir = tempfile.mkdtemp()

        return

    def tearDown(self):
        # teardown the temp dir
        if hasattr(self, 'dir') and self.dir is not None and os.path.exists(self.dir):
            shutil.rmtree(self.dir)
        return

    def testValidateRequest(self):
        raise NotImplementedError


    def testHandleRequest(self):
        raise NotImplementedError

    # END class TestUploadHandler
    pass



class TestHashDataHandler(TestBaseHandler):

    def initializeObjectToTest(self):
        mock.setupMockFunctionsOnClass(cryptic.webapp.HashDataHandler, ['__init__'], {})
        self.objectToTest = cryptic.webapp.HashDataHandler()
        return


    def testHandleRequest(self):

        dataToHash = 'data to hash'
        mock.setupMockFunctionsOnObject(self.objectToTest, ['get_argument'], {'get_argument':dataToHash})

        hashedData = 'hashed data'

        mockCryptic = mock.Mock()
        mock.setupMockFunctionsOnObject(mockCryptic, ['hashData'], {'hashData':hashedData})
        self.objectToTest.cryptic = mockCryptic

        response = {}

        self.objectToTest.handleRequest(response)

        expectedResponse = {
            'status':'success',
            'result':hashedData
            }
        self.assertEquals(expectedResponse, response)

        return

    # END class TestHashDataHandler
    pass




class TestVerifyHashHandler(TestBaseHandler):

    def initializeObjectToTest(self):
        mock.setupMockFunctionsOnClass(cryptic.webapp.VerifyHashHandler, ['__init__'], {})
        self.objectToTest = cryptic.webapp.VerifyHashHandler()
        return


    def testHandleRequest(self):

        dataToHash = 'data to hash'
        hashedData = 'hashed data'

        mock.setupMockFunctionsOnObject(self.objectToTest, ['get_argument'], {'get_argument':hashedData})

        mockCryptic = mock.Mock()
        mock.setupMockFunctionsOnObject(mockCryptic, ['hashData'], {'hashData':hashedData})
        self.objectToTest.cryptic = mockCryptic

        response = {}

        self.objectToTest.handleRequest(response)

        expectedResponse = {
            'status':'success',
            'result':True
            }
        self.assertEquals(expectedResponse, response)

        return

    # END class TestHashDataHandler
    pass



class TestEncryptHandler(TestBaseHandler):

    def initializeObjectToTest(self):
        mock.setupMockFunctionsOnClass(cryptic.webapp.EncryptHandler, ['__init__'], {})
        self.objectToTest = cryptic.webapp.EncryptHandler()
        return


    def testHandleRequest(self):

        dataToEncrypt = 'data to encrypt'
        encryptedData = 'encrypted data'

        mock.setupMockFunctionsOnObject(self.objectToTest, ['get_argument'], {'get_argument':dataToEncrypt})

        mockCryptic = mock.Mock()
        mock.setupMockFunctionsOnObject(mockCryptic, ['encryptData'], {'encryptData':encryptedData})
        self.objectToTest.cryptic = mockCryptic

        response = {}

        self.objectToTest.handleRequest(response)

        expectedResponse = {
            'status':'success',
            'result':encryptedData
            }
        self.assertEquals(expectedResponse, response)

        return

    # END class TestEncryptDataHandler
    pass


class TestDecryptHandler(TestBaseHandler):

    def initializeObjectToTest(self):
        mock.setupMockFunctionsOnClass(cryptic.webapp.DecryptHandler, ['__init__'], {})
        self.objectToTest = cryptic.webapp.DecryptHandler()
        return


    def testHandleRequest(self):

        dataToDecrypt = 'data to decrypt'
        decryptedData = 'decrypted data'

        mock.setupMockFunctionsOnObject(self.objectToTest, ['get_argument'], {'get_argument':dataToDecrypt})

        mockCryptic = mock.Mock()
        mock.setupMockFunctionsOnObject(mockCryptic, ['decryptData'], {'decryptData':decryptedData})
        self.objectToTest.cryptic = mockCryptic

        response = {}

        self.objectToTest.handleRequest(response)

        expectedResponse = {
            'status':'success',
            'result':decryptedData
            }
        self.assertEquals(expectedResponse, response)

        return

    # END class TestDecryptDataHandler
    pass
