import functools


class Mock(object):

    # END class Mock
    pass


def mockFunction(calls, *args, **kwds):
    returnValue = None
    if '__returnValue' in kwds:
        returnValue = kwds['__returnValue']
        del kwds['__returnValue']
        
    calls.append((args, kwds))
    return returnValue


def setupMockFunctionsOnClass(aClass, functionNames, returnValues):
    for functionName in functionNames:
        calls = []
        returnValue = returnValues.get(functionName, None)
        theMockFunction = functools.partial(mockFunction, calls, __returnValue=returnValue)
        setattr(aClass, functionName, theMockFunction)
        pass
    return


def setupMockFunctionsOnObject(anObject, functionNames, returnValues):
    allCalls = {}
    for functionName in functionNames:
        calls = []
        allCalls[functionName] = calls

        returnValue = returnValues.get(functionName, None)
        theMockFunction = functools.partial(mockFunction, calls, __returnValue=returnValue)
        setattr(anObject, functionName, theMockFunction)
        pass
    anObject.calls = allCalls
    return

def verifyExpectedCalls(aTestCase, anObject, expectedCalls):
    actualCalls = anObject.calls
    for key in actualCalls:
        aTestCase.assertTrue(key in expectedCalls, "did not expect %s to be called" % key)
        pass
    for key in expectedCalls:
        aTestCase.assertTrue(key in actualCalls, "expected %s to be called" % key)
        expectedFunctionCalls = expectedCalls[key]
        actualFunctionCalls = actualCalls[key]
        aTestCase.assertEquals(len(expectedFunctionCalls), len(actualFunctionCalls), "expected same number of calls to %s" % key)
        for expectedFunctionCall, actualFunctionCall in zip(expectedFunctionCalls, actualFunctionCalls):
            expectedArgs = expectedFunctionCall[0]
            actualArgs = actualFunctionCall[0]
            # compare the args
            aTestCase.assertEquals(expectedArgs, actualArgs, "expect arguments %s to be same" % key)

            expectedKwds = expectedFunctionCall[1]
            actualKwds = actualFunctionCall[1]
            # compare the kwds
            aTestCase.assertEquals(expectedKwds, actualKwds, "expected keywords %s to be same" % key)
            pass
        pass
    return
