import unittest

import filecmp
import hashlib
import json
import os
import shutil
import tempfile

import StringIO

import cryptic

class TestCryptic(unittest.TestCase):

    def setUp(self):
        self.dir = tempfile.mkdtemp()
        theCryptic = cryptic.Cryptic()
        theCryptic.name('unittest')
        theCryptic.location(self.dir)
        theCryptic.initializeStorageBackend()
        theCryptic.initialize()
        self.cryptic = theCryptic
        return

    def tearDown(self):
        # teardown the temp dir
        if hasattr(self, 'dir') and self.dir is not None and os.path.exists(self.dir):
            shutil.rmtree(self.dir)
        return


    def testConstructEncryptedPath(self):

        context = {}

        expected = os.path.join(self.cryptic.fileDir(), self.cryptic.hashData(json.dumps(context)))
        actual = path = self.cryptic.constructEncryptedPath(context)
        self.assertEquals(expected, actual)

        return


    def testStorageBackend(self):

        self.assertTrue(os.path.exists(self.cryptic.fileDir()))
        self.assertTrue(os.path.exists(self.cryptic.keyDir()))

        return


    def testStoreAndRetrieveFile(self):

        filePath = os.path.join('docs', 'keyczar05b.pdf')
        context = {'foo':'bar', 'test':'store and retrieve'}
        self.cryptic.storeFile(filePath, context=context)
        
        decryptedFh, decryptedFilePath = tempfile.mkstemp(dir=self.cryptic.location())
        self.cryptic.retrieveFile(decryptedFilePath, context=context)

        self.assertTrue(filecmp.cmp(filePath, decryptedFilePath))
        self.assertFalse(filecmp.cmp(filePath, self.cryptic.constructEncryptedPath(context)))

        return

    def testHasData(self):
        filePath = os.path.join('docs', 'keyczar05b.pdf')
        context = {'foo':'bar', 'test':'has data'}
        self.cryptic.storeFile(filePath, context=context)
        self.assertTrue(self.cryptic.hasData(context))
        return


    def testStoreToExisting(self):

        filePath = os.path.join('docs', 'keyczar05b.pdf')
        context = {'foo':'bar', 'test':'store to existing'}
        self.cryptic.storeFile(filePath, context=context)

        self.assertRaises(self.cryptic.storeFile(filePath, context=context))
        return


    def testRemoveData(self):

        filePath = os.path.join('docs', 'keyczar05b.pdf')
        context = {'foo':'bar', 'test':'remove data'}
        self.cryptic.storeFile(filePath, context=context)
        self.assertTrue(self.cryptic.hasData(context))

        self.cryptic.removeData(context)
        self.assertFalse(self.cryptic.hasData(context))

        return


    def testHashData(self):
        testString = 'foo bar'
        expected = hashlib.sha512(testString).hexdigest()
        actual = self.cryptic.hashData(testString)
        self.assertEquals(expected, actual)
        return


    def testRoundtripEncrypt(self):
        testString = 'foo bar'
        encrypted = self.cryptic.encryptData(testString)
        decrypted = self.cryptic.decryptData(encrypted)

        self.assertEquals(testString, decrypted)

        self.assertFalse(testString == encrypted)
        return



    # END class TestCryptic
    pass
