import os
from setuptools import setup
from setuptools import find_packages

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "cryptic",
    version = "0.0.1",
    author = "Michael Pan",
    author_email = "mikepan@gmail.com",
    description = ("web service for storing encrypted files"),
    license = "BSD",
    keywords = "encryption",
    url = "http://bitbucket.org/mjpan/cryptic",
    packages=find_packages(),
    long_description=read('README'),
    classifiers=[],
    test_suite='nose.collector',
    tests_require=['nose']
)
